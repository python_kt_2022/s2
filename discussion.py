# Control Structures in Python

# username = input("Please enter your username:\n");
# print(f"Hello {username}! Welcome to Python Short Course.")

# num1 = input(int("Enter the first number:\n"));
# num2 = input(int("Enter the second number:\n"));
# print(f"The sum of num1 and num2 is {num1 + num2}");

# [Section] If-else statements
# test_num = 75;

# if test_num >= 68:
# 	print("Test Passed");
# else :
# 	print("Test Failed");

# # [Section] if-else chains
# test_num2 = int(input("Please enter number:\n"))

# if test_num > 0:
# 	print("The number is positive");
# elif test_num2 == 0:
# 	print("The number is zero");
# else: 
# 	print("The number is negative");

# mini act to test divisibility 3, 5
# test_div_num = int(input("Please enter a number to test\n"))
# if test_div_num % 3 == 0 and test_div_num % 5 == 0:
#     print(f"{test_div_num} is divisible by both 3 and 5")
# elif test_div_num % 3 == 0:
#     print(f"{test_div_num} is divisible by 3")
# elif test_div_num % 5 == 0:
#     print(f"{test_div_num} is divisible by 5")
# else:
#     print(f"{test_div_num} is not divisible by both 3 nor 5")



# [Section] Loops
# [Section] While Loops
# i = 1;
# while i <= 5 : 
# 	print(f"Current value: {i}")
# 	i += 1

# [Section] For Loops
# iterating over/through a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

# using range()
# for y in range(6,10,2):
# 	print(f"The current value is {y}");
# for y in range(6,10):
# 	print(f"the current value is {y}");
# for y in range(20, 6, -3):
# 	print(f"The current value is {y}");


# [Section] break statements
j = 1
while j < 6:
	print(j)
	if j == 3: 
		break
	j += 1

# [Section] Continue Statement
k = 1
while k < 6: 
	if k == 3:
		continue
	k += 1
	print(k)

